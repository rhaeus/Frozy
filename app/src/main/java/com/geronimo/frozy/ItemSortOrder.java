package com.geronimo.frozy;

public enum ItemSortOrder {
    NAME_ASC,
    NAME_DESC,
    DATE_ASC,
    DATE_DESC
}
