package com.geronimo.frozy;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by Ramona on 22.03.2018.
 */

// A top-level Java class mimicking static class behavior
public final class AppData { //final - Prevents extension of the class since extending a static class makes no sense

    private final static String APPDATA_FILE_NAME = "FrozyData.xml";
    private static FreezerManager _freezerManager = new FreezerManager();

    private AppData(){
    }

    public static FreezerManager getFreezerManager(){
        return _freezerManager;
    }



    public static boolean readAppData(String filePath){
        Serializer serializer = new Persister();
        _freezerManager = new FreezerManager(); //clear

        try {
            FileInputStream fileIn = new FileInputStream(filePath + "/" + APPDATA_FILE_NAME);
            _freezerManager = serializer.read(FreezerManager.class, fileIn);
            return true;
        }catch(Exception e){
            String s = e.getMessage();
            _freezerManager.autoGenerateFreezer();//TODO weg
            return false;
        }
    }

    public static boolean writeAppData(String filePath){
        Serializer serializer = new Persister();
        File result = new File(filePath + "/" + APPDATA_FILE_NAME);

        try {
            FileOutputStream fos = new FileOutputStream(result);
            serializer.write(_freezerManager, fos);
            return true;
        }catch(Exception e){
            String s = e.getMessage();
            return false;
        }
    }
}
