package com.geronimo.frozy;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ShowItemDialogFragment extends DialogFragment {
    FrozenItem item;

    public ShowItemDialogFragment(){ }

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    static ShowItemDialogFragment newInstance(int itemIndex) {
        ShowItemDialogFragment f = new ShowItemDialogFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("itemIndex", itemIndex);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Dialog);
        int pos = getArguments().getInt("itemIndex");
        item = AppData.getFreezerManager().getFrozenItem(pos);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_view_item, container, false);
        TextView tv = v.findViewById(R.id.view_name);
        tv.setText(item.Name);

        tv = v.findViewById(R.id.view_date);
        tv.setText(item.getDateString("dd. MMMM YYYY"));

        tv = v.findViewById(R.id.view_dest_freezer);
        tv.setText(item.getFreezerName());

        tv = v.findViewById(R.id.view_dest_drawer);
        tv.setText(item.getDrawerName());

        tv = v.findViewById(R.id.view_amount);
        tv.setText(item.getAmountString());

        tv = v.findViewById(R.id.view_amount);
        tv.setText(item.getAmountString());

        tv = v.findViewById(R.id.view_amount_unit);
        String[] arr = getActivity().getResources().getStringArray(R.array.item_amount_units);
        tv.setText(arr[item.ItemUnit.getValue()]);

        tv = v.findViewById(R.id.view_comment);
        tv.setText(item.Comment);

        ImageView back = v.findViewById(R.id.view_back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                back();
            }
        });


//        // Watch for button clicks.
//        Button button = (Button)v.findViewById(R.id.show);
//        button.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                // When button is clicked, call up to owning activity.
//                ((FragmentDialog)getActivity()).showDialog();
//            }
//        });

        return v;
    }

    private void back()
    {
        close(); //TODO save changes
    }

    private void close() {
        getFragmentManager().beginTransaction().remove(this).commit();
    }

}
