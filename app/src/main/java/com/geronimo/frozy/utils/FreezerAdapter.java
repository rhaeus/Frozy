package com.geronimo.frozy.utils;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geronimo.frozy.Freezer;
import com.geronimo.frozy.FrozenItem;
import com.geronimo.frozy.R;

import java.util.ArrayList;

/**
 * Created by Ramona on 23.03.2018.
 */

public class FreezerAdapter extends RecyclerView.Adapter<FreezerAdapter.ItemViewHolder>{
    private ArrayList<Freezer> _itemList;
    ItemClickListener _listener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout ViewBackground;
        public RelativeLayout ViewForeground;
        protected  TextView _line1_left;
        protected  TextView _line2;
        protected  TextView _line1_right;

        public ItemViewHolder(View v) {
            super(v);
            ViewBackground = v.findViewById(R.id.view_background);
            ViewForeground = v.findViewById(R.id.view_foreground);

            _line1_left = (TextView) v.findViewById(R.id.line_name);
            _line2 = (TextView) v.findViewById(R.id.line_position);
            _line1_right = (TextView) v.findViewById(R.id.line_date);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FreezerAdapter(ArrayList<Freezer> itemList) {
        _itemList = itemList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.list_two_lines, parent, false);

        final ItemViewHolder mViewHolder = new ItemViewHolder(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _listener.onItemClick(v, mViewHolder.getLayoutPosition());
            }
        });
        return mViewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Freezer item = _itemList.get(position);
        holder._line1_left.setText(item.Name);
        holder._line2.setText(item.getDrawerNames());
        holder._line1_right.setText(Integer.toString(item.getDrawerCount())+ " Fächer");
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return _itemList.size();
    }

    public void setOnItemClickListener(ItemClickListener listener)
    {
        _listener = listener;
    }

    public Freezer getItem(int position){
        return _itemList.get(position);
    }

}
