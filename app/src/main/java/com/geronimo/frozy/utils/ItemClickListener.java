package com.geronimo.frozy.utils;

import android.view.View;

import com.geronimo.frozy.FrozenItem;

public interface ItemClickListener {
    public void onItemClick(View v, int position);

//    public void onItemClick(View v, FrozenItem clickedItem);
}
