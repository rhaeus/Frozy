package com.geronimo.frozy.utils;


import android.support.v7.widget.RecyclerView;


/**
 * Created by Ramona on 22.03.2018.
 */

public abstract class MyRecyclerScroll extends RecyclerView.OnScrollListener {
    protected static final float MINIMUM = 25;

    protected int _scrollDist = 0;
    protected boolean _isVisible = true;

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        //dy:   scrolling down -> positive
        //      scrollung up -> negative

        if (_isVisible && _scrollDist > MINIMUM) {
            hide();
            _scrollDist = 0;
            _isVisible = false;
        }
        else if (!_isVisible && _scrollDist < -MINIMUM) {
            show();
            _scrollDist = 0;
            _isVisible = true;
        }

        if ((_isVisible && dy > 0) || (!_isVisible && dy < 0)) {
            _scrollDist += dy;
        }

    }

    public abstract void show();
    public abstract void hide();

}
