package com.geronimo.frozy.utils;

import com.geronimo.frozy.FrozenItem;
import com.geronimo.frozy.ItemSortOrder;

import java.util.Comparator;

public class FrozenItemComparator implements Comparator<FrozenItem>{
    private ItemSortOrder _sortOrder;
    public FrozenItemComparator(ItemSortOrder sortOrder){
        _sortOrder = sortOrder;
    }

    @Override
    public int compare(FrozenItem o1, FrozenItem o2) {
        switch(_sortOrder){
            case NAME_ASC:
                return o1.Name.compareTo(o2.Name);
            case NAME_DESC:
                return o2.Name.compareTo(o1.Name);
            case DATE_ASC:
                return o1.FrozenDate.compareTo(o2.FrozenDate);
            case DATE_DESC:
                return o2.FrozenDate.compareTo(o1.FrozenDate);
            default:
                return 0; //returns a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
        }
    }
}
