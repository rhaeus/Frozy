package com.geronimo.frozy.utils;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geronimo.frozy.AppData;
import com.geronimo.frozy.FrozenItem;
import com.geronimo.frozy.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ramona on 23.03.2018.
 */

public class FrozenItemAdapter extends RecyclerView.Adapter<FrozenItemAdapter.ItemViewHolder>{
//    private ArrayList<FrozenItem> _sortedList;
    ItemClickListener _listener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout ViewBackground;
        public RelativeLayout ViewForeground;
        protected  TextView _lineName;
        protected  TextView _linePosition;
        protected  TextView _lineDate;

        public ItemViewHolder(View v) {
            super(v);
            ViewBackground = v.findViewById(R.id.view_background);
            ViewForeground = v.findViewById(R.id.view_foreground);

            _lineName = (TextView) v.findViewById(R.id.line_name);
            _linePosition = (TextView) v.findViewById(R.id.line_position);
            _lineDate = (TextView) v.findViewById(R.id.line_date);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FrozenItemAdapter(ArrayList<FrozenItem> itemList) {
        add(itemList);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.list_two_lines, parent, false);

        final ItemViewHolder mViewHolder = new ItemViewHolder(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _listener.onItemClick(v, mViewHolder.getLayoutPosition());
            }
        });
        return mViewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        FrozenItem item = _sortedList.get(position);
        holder._lineName.setText(item.Name);
        holder._linePosition.setText(item.getPositionString());
        holder._lineDate.setText(item.getDateString("dd.MM.yy"));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return _sortedList.size();
    }

    public void setOnItemClickListener(ItemClickListener listener)
    {
        _listener = listener;
    }

    public FrozenItem getItem(int position){
        return _sortedList.get(position);
    }

    private final SortedList<FrozenItem> _sortedList = new SortedList<>(FrozenItem.class, new SortedList.Callback<FrozenItem>() {
        @Override
        public int compare(FrozenItem a, FrozenItem b) {
            return new FrozenItemComparator(AppData.getFreezerManager().getLastSortOrder()).compare(a,b);
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(FrozenItem oldItem, FrozenItem newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(FrozenItem item1, FrozenItem item2) {
            return item1.getId() == item2.getId();
        }
    });

    public void add(FrozenItem model) {
        _sortedList.add(model);
    }

    public void remove(FrozenItem model) {
        _sortedList.remove(model);
    }

    public void add(List<FrozenItem> models) {
        _sortedList.addAll(models);
    }

    public void remove(List<FrozenItem> models) {
        _sortedList.beginBatchedUpdates();
        for (FrozenItem model : models) {
            _sortedList.remove(model);
        }
        _sortedList.endBatchedUpdates();
    }

    public void renewList(List<FrozenItem> models) {
        _sortedList.beginBatchedUpdates();
        _sortedList.clear();
        _sortedList.addAll(models);
        _sortedList.endBatchedUpdates();
    }

    public void replaceAll(List<FrozenItem> models) {
        _sortedList.beginBatchedUpdates();
        for (int i = _sortedList.size() - 1; i >= 0; i--) {
            final FrozenItem model = _sortedList.get(i);
            if (!models.contains(model)) {
                _sortedList.remove(model);
            }
        }
        _sortedList.addAll(models);
        _sortedList.endBatchedUpdates();
    }

}
