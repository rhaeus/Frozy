package com.geronimo.frozy;

//import android.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import android.support.v4.app.FragmentManager;

public class MainActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener
{

    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //registerNavigationClickEvents();
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        if(savedInstanceState == null) {
            navigationView.getMenu().getItem(0).setChecked(true);
//            showFragment(R.id.nav_all);
            setTitle(R.string.nav_item_all);
            Fragment_All all = new Fragment_All();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, all).commit();
        }
    }

//    private void registerNavigationClickEvents(){
//        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
//
//        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//        navigationView.getMenu().getItem(0).setChecked(true);
//        showFragment(R.id.nav_all);
////        navigationView.setNavigationItemSelectedListener(
////                new NavigationView.OnNavigationItemSelectedListener() {
////                    @Override
////                    public boolean onNavigationItemSelected(MenuItem menuItem) {
////                        // set item as selected to persist highlight
////                        menuItem.setChecked(true);
////                        // close drawer when item is tapped
////                        mDrawerLayout.closeDrawers();
////
////                        // Add code here to update the UI based on the item selected
////                        // For example, swap UI fragments here
////
////                        return true;
////                    }
////                });
//    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // set item as selected to persist highlight
        menuItem.setChecked(true);
        // close drawer when item is tapped
        mDrawerLayout.closeDrawers();

        showFragment(menuItem.getItemId());

        return true;
    }

    private void showFragment(int menuID)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch(menuID) {
            case R.id.nav_all:
                setTitle(R.string.nav_item_all);
                Fragment_All all = new Fragment_All();
                fragmentManager.beginTransaction().replace(R.id.content_frame, all).addToBackStack("ALL").commit();
                break;
            case R.id.nav_manage_freezer:
                setTitle(R.string.nav_item_manage_freezer);
                Fragment_Manage_Freezer manage = new Fragment_Manage_Freezer();
                fragmentManager.beginTransaction().replace(R.id.content_frame, manage).addToBackStack("MANAGE_FREEZER").commit();
                break;
//            case R.id.nav_export:
//                break;
//            case R.id.nav_settings:
//                setTitle(R.string.nav_item_settings);
//                Fragment_Settings settings = new Fragment_Settings();
//                fragmentManager.beginTransaction().replace(R.id.content_frame, settings).addToBackStack("SETTINGS").commit();
//                break;
            case R.id.nav_info:
                setTitle(R.string.nav_item_info);
                Fragment_About about = new Fragment_About();
                fragmentManager.beginTransaction().replace(R.id.content_frame, about).addToBackStack("ABOUT").commit();;
                break;
        }
    }

    //Open the drawer when the button is tapped
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop(){
        super.onStop();

        AppData.writeAppData(getFilesDir().getAbsolutePath());
    }

    @Override
    public void onStart(){
        super.onStart();

        AppData.readAppData(getFilesDir().getAbsolutePath());
        //AppData.getFreezerManager().autoGenerateFreezer();
    }
}
