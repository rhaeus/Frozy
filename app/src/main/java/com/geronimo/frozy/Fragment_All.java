package com.geronimo.frozy;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.geronimo.frozy.utils.FrozenItemAdapter;
import com.geronimo.frozy.utils.ItemClickListener;
import com.geronimo.frozy.utils.FrozenItemTouchHelper;
import com.geronimo.frozy.utils.MyRecyclerScroll;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_All extends Fragment
        implements FrozenItemTouchHelper.FrozenItemTouchHelperListener, SearchView.OnQueryTextListener
{
    private RecyclerView _recyclerView;
    //private RecyclerView.Adapter _recyclerViewAdapter;
    public static FrozenItemAdapter _recyclerViewAdapter;
    private RecyclerView.LayoutManager _layoutManager;
    private FloatingActionButton _fab;
    private View _fragmentView;

    public Fragment_All() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _fragmentView = inflater.inflate(R.layout.fragment_all, container, false);

        setHasOptionsMenu(true);
        _fab = _fragmentView.findViewById(R.id.fab);
        _recyclerView = (RecyclerView) _fragmentView.findViewById(R.id.listTwoLines);

        setUpFABBehavior();

        _fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddItemDialog();
            }
        });

        return _fragmentView;
    }



    @Override
    public void onResume(){
        super.onResume();
        showList(_fragmentView);
    }

    private void showList(View view){

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        _recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        _layoutManager = new LinearLayoutManager(getContext());
        _recyclerView.setLayoutManager(_layoutManager);
        _recyclerView.setItemAnimator(new DefaultItemAnimator());

        //divider
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(_recyclerView.getContext(), LinearLayoutManager.VERTICAL);
        _recyclerView.addItemDecoration(dividerItemDecoration);

        // specify an adapter
        _recyclerViewAdapter = new FrozenItemAdapter(AppData.getFreezerManager().getFrozenItemList());
        _recyclerViewAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                showViewItemDialog(position);
                //Toast.makeText(getContext(),"click: "+ _recyclerViewAdapter.getItem(position).Name, Toast.LENGTH_SHORT).show();
//                _recyclerViewAdapter.getItem(position).Name = "Geklickt";
//                _recyclerViewAdapter.notifyItemChanged(position);

            }
        });
        _recyclerView.setAdapter(_recyclerViewAdapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new FrozenItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(_recyclerView);
    }

    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof FrozenItemAdapter.ItemViewHolder) {
            // backup of removed item for undo purpose
            final int deletedIndex = viewHolder.getAdapterPosition();
            final FrozenItem deletedItem = AppData.getFreezerManager().getFrozenItemList().get(deletedIndex);

            // remove the item
            AppData.getFreezerManager().removeFrozenItem(viewHolder.getAdapterPosition());
            //_recyclerViewAdapter.notifyItemRemoved(deletedIndex);

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(_fragmentView, getResources().getString(R.string.item_removed), Snackbar.LENGTH_LONG);
            snackbar.setAction(getResources().getString(R.string.undo), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // undo is selected, restore the deleted item
                   // _recyclerViewAdapter.notifyItemInserted(AppData.getFreezerManager().addFrozenItem(deletedItem));
                    AppData.getFreezerManager().addFrozenItem(deletedItem);
                }
            });
            snackbar.show();
        }
    }

    private void setUpFABBehavior(){

        //hide upon scrolling down and reappear when scrolled up (known as the Quick Return Pattern)

        final int fabMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        _recyclerView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                _fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                _fab.animate().translationY(_fab.getHeight() + fabMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });
    }

    private void showAddItemDialog(){
        FragmentManager fragmentManager = getChildFragmentManager();
        AddItemDialogFragment dialog = new AddItemDialogFragment();
        dialog.show(fragmentManager, "AddItemDialog");

//        FragmentManager fragmentManager = getChildFragmentManager();
//        AddItemDialogFragment newFragment = new AddItemDialogFragment();
//
//            // The device is smaller, so show the fragment fullscreen
//            FragmentTransaction transaction = fragmentManager.beginTransaction();
//            // For a little polish, specify a transition animation
//            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//            // To make it fullscreen, use the 'content' root view as the container
//            // for the fragment, which is always the root view for the activity
//            transaction.add(R.id.addItem, newFragment)
//                    .addToBackStack(null).commit();
////        transaction.add(android.R.id.content, newFragment)
////                .addToBackStack(null).commit();
        }

    private void showViewItemDialog(int itemIndex) {
        FragmentManager fragmentManager = getChildFragmentManager();
        ShowItemDialogFragment dialog = ShowItemDialogFragment.newInstance(itemIndex);
        dialog.show(fragmentManager, "ViewItemDialog");
    }

    public void addItem(FrozenItem itemToAdd){
        int position = AppData.getFreezerManager().addFrozenItem(itemToAdd);
        //_recyclerViewAdapter.replaceAll(AppData.getFreezerManager().getFrozenItemList());
//        _recyclerViewAdapter.notifyItemChanged(position);
        _recyclerView.getLayoutManager().scrollToPosition(position);
        Snackbar.make(_fragmentView, R.string.item_added, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.fragment_all_toolbar_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //search function
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }
    //search function
    @Override
    public boolean onQueryTextChange(String newText) {
        final List<FrozenItem> filteredModelList = filter(AppData.getFreezerManager().getFrozenItemList(), newText);
        _recyclerViewAdapter.replaceAll(filteredModelList);
        _recyclerView.scrollToPosition(0);
        return true;
    }

    private static List<FrozenItem> filter(List<FrozenItem> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<FrozenItem> filteredModelList = new ArrayList<>();
        for (FrozenItem model : models) {
            final String name = model.Name.toLowerCase();
            final String comment = model.Comment.toLowerCase();
            if (name.contains(lowerCaseQuery) || comment.contains(lowerCaseQuery)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort:
                switch(AppData.getFreezerManager().getLastSortOrder()){
                    case NAME_ASC:
                        item.getSubMenu().findItem(R.id.menu_sort_name_asc).setChecked(true);
                        break;
                    case NAME_DESC:
                        item.getSubMenu().findItem(R.id.menu_sort_name_desc).setChecked(true);
                        break;
                    case DATE_ASC:
                        item.getSubMenu().findItem(R.id.menu_sort_date_asc).setChecked(true);
                        break;
                    case DATE_DESC:
                        item.getSubMenu().findItem(R.id.menu_sort_date_desc).setChecked(true);
                        break;
                }
                return true;

            case R.id.menu_sort_name_asc:
                AppData.getFreezerManager().sortItems(ItemSortOrder.NAME_ASC);
                _recyclerViewAdapter.notifyDataSetChanged();
                return true;

            case R.id.menu_sort_name_desc:
                AppData.getFreezerManager().sortItems(ItemSortOrder.NAME_DESC);
                _recyclerViewAdapter.notifyDataSetChanged();
                return true;

            case R.id.menu_sort_date_asc:
                AppData.getFreezerManager().sortItems(ItemSortOrder.DATE_ASC);
                _recyclerViewAdapter.notifyDataSetChanged();
                return true;

            case R.id.menu_sort_date_desc:
                AppData.getFreezerManager().sortItems(ItemSortOrder.DATE_DESC);
                _recyclerViewAdapter.notifyDataSetChanged();
                return true;

            case R.id.search:

                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}
