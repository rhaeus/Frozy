package com.geronimo.frozy;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.geronimo.frozy.utils.FreezerAdapter;
import com.geronimo.frozy.utils.FrozenItemAdapter;
import com.geronimo.frozy.utils.ItemClickListener;
import com.geronimo.frozy.utils.FrozenItemTouchHelper;
import com.geronimo.frozy.utils.MyRecyclerScroll;

public class Fragment_Manage_Freezer extends Fragment {
    private RecyclerView _recyclerView;
    private FreezerAdapter _recyclerViewAdapter;
    private RecyclerView.LayoutManager _layoutManager;
    private FloatingActionButton _fab;
    private View _fragmentView;

    public Fragment_Manage_Freezer(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _fragmentView = inflater.inflate(R.layout.fragment_manage_freezer, container, false);

        setHasOptionsMenu(true);
        _fab = _fragmentView.findViewById(R.id.fab_addFreezer);
        _recyclerView = (RecyclerView) _fragmentView.findViewById(R.id.listTwoLines);

        setUpFABBehavior();

//        _fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showAddItemDialog();
//            }
//        });

        return _fragmentView;
    }

    private void setUpFABBehavior(){

        //hide upon scrolling down and reappear when scrolled up (known as the Quick Return Pattern)

        final int fabMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        _recyclerView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                _fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                _fab.animate().translationY(_fab.getHeight() + fabMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        showList(_fragmentView);
    }

    private void showList(View view){

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        _recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        _layoutManager = new LinearLayoutManager(getContext());
        _recyclerView.setLayoutManager(_layoutManager);
        _recyclerView.setItemAnimator(new DefaultItemAnimator());

        //divider
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(_recyclerView.getContext(), LinearLayoutManager.VERTICAL);
        _recyclerView.addItemDecoration(dividerItemDecoration);

        // specify an adapter
        _recyclerViewAdapter = new FreezerAdapter(AppData.getFreezerManager().getFreezerList());
        _recyclerViewAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Toast.makeText(getContext(),"click: "+ _recyclerViewAdapter.getItem(position).Name, Toast.LENGTH_SHORT).show();
//                _recyclerViewAdapter.getItem(position).Name = "Geklickt";
//                _recyclerViewAdapter.notifyItemChanged(position);

            }
        });
        _recyclerView.setAdapter(_recyclerViewAdapter);

//        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new FrozenItemTouchHelper(0, ItemTouchHelper.LEFT, this);
//        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(_recyclerView);
    }
}
