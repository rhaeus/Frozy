package com.geronimo.frozy;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geronimo.frozy.utils.CustomSpinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Ramona on 23.03.2018.
 */

public class AddItemDialogFragment extends DialogFragment {
    TextView _tvItemName;
    LinearLayout _llNameErrorLayout;
    TextView _tvItemDate;

    CustomSpinner _cspFreezer;
    LinearLayout _llFreezerErrorLayout;
    CustomSpinner _cspDrawer;
    LinearLayout _llDrawerErrorLayout;
    TextView _tvDrawer;
    TextView _tvFreezer;

    TextView _tvItemAmount;
    LinearLayout _llAmountErrorLayout;
    CustomSpinner _cspAmountUnit;
    TextView _tvUnit;

    CustomSpinner _cspState;
    TextView _tvState;

    TextView _tvItemComment;

    ImageView _cancel;
    TextView _save;

    final Calendar _selectedCalendar = Calendar.getInstance();



    /** The system calls this to get the DialogFragment's layout, regardless
     of whether it's being displayed as a dialog or an embedded fragment. */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view = inflater.inflate(R.layout.layout_add_item, container, false);

        View v = view.findViewById(R.id.test);
        v.setEnabled(false);

        ArrayAdapter<String> arrayAdapter;

        _tvItemName = view.findViewById(R.id.item_name);
        _llNameErrorLayout = view.findViewById(R.id.NameErrorLayout);

        //date input
        _tvItemDate = view.findViewById(R.id.item_date);
        updateDateLabel(Calendar.getInstance());
        _tvItemDate.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    selectDate();
                }
            });


            //freezer spinner
            _cspFreezer = view.findViewById(R.id.spinnerFreezer);
            _llFreezerErrorLayout = view.findViewById(R.id.FreezerErrorLayout);
            int freezerCount = AppData.getFreezerManager().getFreezerCount();
            if(freezerCount > 0) {
                String[] FreezerNames = new String[freezerCount];
                for (int i = 0; i < freezerCount; i++) {
                    FreezerNames[i] = AppData.getFreezerManager().getFreezer(i).Name;
                }
                arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, FreezerNames);
                arrayAdapter.setDropDownViewResource(R.layout.spinner_item);
                _cspFreezer.setAdapter(arrayAdapter);
            }

            _tvFreezer = view.findViewById(R.id.freezer_label);
            _cspFreezer.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
                @Override
                public void onSpinnerOpened(AppCompatSpinner spinner) {
                    _tvFreezer.setTextColor(getResources().getColor(R.color.colorAccent, getActivity().getTheme()));
                }

                @Override
                public void onSpinnerClosed(AppCompatSpinner spinner) {
                    _tvFreezer.setTextColor(getResources().getColor(R.color.colorSecondaryText, getActivity().getTheme()));
                }
            });

            _cspFreezer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    populateDrawerSpinnerItems(AppData.getFreezerManager().getFreezer(position));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //drawer spinner
            _cspDrawer = view.findViewById(R.id.spinnerDrawer);
            _llDrawerErrorLayout = view.findViewById(R.id.DrawerErrorLayout);
//            if(freezerCount > 0){
//                populateDrawerSpinnerItems(AppData.getFreezerManager().getFreezer(0));
//            }

            _tvDrawer = view.findViewById(R.id.drawer_label);
            _cspDrawer.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
                @Override
                public void onSpinnerOpened(AppCompatSpinner spinner) {
                    _tvDrawer.setTextColor(getResources().getColor(R.color.colorAccent, getActivity().getTheme()));
                }

                @Override
                public void onSpinnerClosed(AppCompatSpinner spinner) {
                    _tvDrawer.setTextColor(getResources().getColor(R.color.colorSecondaryText, getActivity().getTheme()));
                }
            });

            _tvItemAmount = view.findViewById(R.id.item_amount);
            _llAmountErrorLayout = view.findViewById(R.id.AmountErrorLayout);

            //amount unit spinner
            _cspAmountUnit = view.findViewById(R.id.spinnerAmountUnit);
            arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, getActivity().getResources().getStringArray(R.array.item_amount_units));
            _cspAmountUnit.setAdapter(arrayAdapter);

            _tvUnit = view.findViewById(R.id.item_amount_unit_label);
            _cspAmountUnit.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
                @Override
                public void onSpinnerOpened(AppCompatSpinner spinner) {
                    _tvUnit.setTextColor(getResources().getColor(R.color.colorAccent, getActivity().getTheme()));
                }

                @Override
                public void onSpinnerClosed(AppCompatSpinner spinner) {
                    _tvUnit.setTextColor(getResources().getColor(R.color.colorSecondaryText, getActivity().getTheme()));
                }
            });

            //state
            _cspState = view.findViewById(R.id.spinnerItemState);
            arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, getActivity().getResources().getStringArray(R.array.item_states));
            _cspState.setAdapter(arrayAdapter);

            _tvState = view.findViewById(R.id.item_state_label);
            _cspState.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
                @Override
                public void onSpinnerOpened(AppCompatSpinner spinner) {
                    _tvState.setTextColor(getResources().getColor(R.color.colorAccent, getActivity().getTheme()));
                }

                @Override
                public void onSpinnerClosed(AppCompatSpinner spinner) {
                    _tvState.setTextColor(getResources().getColor(R.color.colorSecondaryText, getActivity().getTheme()));
                }
            });

            _tvItemComment = view.findViewById(R.id.item_comment);

            //cancel icon
            _cancel = view.findViewById(R.id.cancelAddItem);
            _cancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    cancel();
                }
            });

            //save icon
            _save = view.findViewById(R.id.saveItem);
            _save.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    save();
                }
            });


        return view;
    }

    private boolean populateDrawerSpinnerItems(Freezer freezer) {
        if(freezer == null){
            return false;
        }

        int count = freezer.getDrawerCount();
        if(count < 0){
            return false;
        }

        String[] DrawerNames = new String[count];
        for(int i = 0; i < count; i++){
            DrawerNames[i] = freezer.getDrawer(i).Name;
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, DrawerNames);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        _cspDrawer.setAdapter(arrayAdapter);
        return true;
    }

    /** The system calls this only when creating the layout in a dialog. */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        //dialog.requestWindowFeature(Window.FEATURE_ACTION_BAR);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Dialog);
    }

    public void selectDate(){


        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                _selectedCalendar.set(Calendar.YEAR, year);
                _selectedCalendar.set(Calendar.MONTH, monthOfYear);
                _selectedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateLabel(_selectedCalendar);
            }

        };

        new DatePickerDialog(getContext(), date, _selectedCalendar
                .get(Calendar.YEAR), _selectedCalendar.get(Calendar.MONTH),
                _selectedCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    private void updateDateLabel(Calendar date){
        String myFormat = "dd. MMMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMANY);

        _tvItemDate.setText(sdf.format(date.getTime()));
    }

    private void close() {
        //Toast.makeText(getContext(), "Cancel", Toast.LENGTH_SHORT).show();
        getFragmentManager().beginTransaction().remove(this).commit();
    }

    private boolean save() {
        if(!checkInput()){
            return false;
        }

        Fragment parentFragment = getParentFragment();
        if(parentFragment instanceof Fragment_All){
            FrozenItem newItem = new FrozenItem(AppData.getFreezerManager().getNewItemID(),
                    _tvItemName.getText().toString(),
                    _selectedCalendar.getTime(),
                    _cspFreezer.getSelectedItemPosition(),
                    _cspDrawer.getSelectedItemPosition(),
                    Double.parseDouble(_tvItemAmount.getText().toString()),
                    Unit.values()[_cspAmountUnit.getSelectedItemPosition()],
                    State.values()[_cspState.getSelectedItemPosition()],
                    _tvItemComment.getText().toString()
                    );
            ((Fragment_All) parentFragment).addItem(newItem);
            close();
            return true;
        }
        return false;
    }

    private boolean checkInput(){
        boolean ok = true;
        //name
        if(_tvItemName.getText().length() <= 0){
            ok = false;
            _llNameErrorLayout.setVisibility(View.VISIBLE);
        }
        else{
            _llNameErrorLayout.setVisibility(View.GONE);
        }

        //freezer
        if(_cspFreezer.getSelectedItemPosition() < 0){
            ok = false;
            _llFreezerErrorLayout.setVisibility(View.VISIBLE);
        }
        else{
            _llFreezerErrorLayout.setVisibility(View.GONE);
        }

        //drawer
        if(_cspDrawer.getSelectedItemPosition() < 0){
            ok = false;
            _llDrawerErrorLayout.setVisibility(View.VISIBLE);
        }
        else{
            _llDrawerErrorLayout.setVisibility(View.GONE);
        }

        //amount
        if(_tvItemAmount.getText().length() <= 0){
            ok = false;
            _llAmountErrorLayout.setVisibility(View.VISIBLE);
        }
        else{
            _llAmountErrorLayout.setVisibility(View.GONE);
        }

        if(!ok){
            Toast.makeText(getContext(),getResources().getString(R.string.input_error),Toast.LENGTH_LONG).show();
        }

        return ok;
    }

    private void cancel()
    {
        close(); //TODO display alert
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        Dialog d = getDialog();
//        if (d!=null){
//            int width = ViewGroup.LayoutParams.MATCH_PARENT;
//            int height = ViewGroup.LayoutParams.MATCH_PARENT;
//            d.getWindow().setLayout(width, height);
//        }
//    }
}
