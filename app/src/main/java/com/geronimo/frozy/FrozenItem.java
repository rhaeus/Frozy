package com.geronimo.frozy;

import android.content.res.Resources;

import org.simpleframework.xml.Element;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by Ramona on 22.03.2018.
 */

enum Unit{
    PORTIONS(0),
    PIECES(1),
    GRAM(2),
    LITRES(3);

    private final int value;
    private Unit(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

enum State{
    FINISHED,
    RAW
}

public class FrozenItem {
    @Element
    private int _id = -1;
    @Element
    public String Name = "";
    @Element
    public Date FrozenDate = new Date();
    @Element
    public int FreezerIndex = -1;
    @Element
    public int DrawerIndex = -1;
    @Element
    public double Amount = -1;
    @Element
    public Unit ItemUnit = Unit.PORTIONS;
    @Element
    public State ItemState = State.FINISHED;
    @Element (required = false)
    public String Comment = "";

    public FrozenItem(){}

    public FrozenItem(int id, String name, Date date, int freezerIndex, int drawerIndex, double amount, Unit unit, State state, String comment) {
        _id = id;
        Name = name;
        FrozenDate = date;
        FreezerIndex = freezerIndex;
        DrawerIndex = drawerIndex;
        Amount = amount;
        ItemUnit = unit;
        ItemState = state;
        Comment = comment;
    }

    public String getPositionString(){
        return getFreezerName() + " | " + getDrawerName();
    }

    public String getDateString(String format){
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.GERMAN);
        return formatter.format(FrozenDate);
    }

    public int getId(){
        return _id;
    }

    public String getFreezerName(){
        if(FreezerIndex >= 0)
            return AppData.getFreezerManager().getFreezer(FreezerIndex).Name;
        else
            return "";
    }

    public String getDrawerName(){
        if(DrawerIndex >= 0)
            return AppData.getFreezerManager().getFreezer(FreezerIndex).getDrawer(DrawerIndex).Name;
        else
            return "";
    }

    public String getAmountString(){
        return String.format("%.0f",Amount);
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FrozenItem that = (FrozenItem) o;
        return _id == that._id &&
                FreezerIndex == that.FreezerIndex &&
                DrawerIndex == that.DrawerIndex &&
                Double.compare(that.Amount, Amount) == 0 &&
                Objects.equals(Name, that.Name) &&
                Objects.equals(FrozenDate, that.FrozenDate) &&
                ItemUnit == that.ItemUnit &&
                ItemState == that.ItemState &&
                Objects.equals(Comment, that.Comment);
    }

    @Override
    public int hashCode() {

        return Objects.hash(_id, Name, FrozenDate, FreezerIndex, DrawerIndex, Amount, ItemUnit, ItemState, Comment);
    }
}
