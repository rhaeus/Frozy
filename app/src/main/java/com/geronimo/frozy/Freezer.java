package com.geronimo.frozy;


import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ramona on 24.03.2018.
 */

public class Freezer {
    @Element
    public String Name = "";
    @ElementList
    private ArrayList<Drawer> _drawerList = new ArrayList<>();

    public Freezer(){}

    public Freezer(String name) {
        Name = name;
    }

    public void addDrawer(Drawer drawer) {
        _drawerList.add(drawer);
    }

    public Drawer getDrawer(int index){
        return _drawerList.get(index);
    }

    public int getDrawerCount(){
        return _drawerList.size();
    }

    public String getDrawerNames(){
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < getDrawerCount();i++){
            if(i < getDrawerCount()-1)
                builder.append(getDrawer(i).Name + " | ");
            else
                builder.append(getDrawer(i).Name);
        }
        return builder.toString();
    }



}
