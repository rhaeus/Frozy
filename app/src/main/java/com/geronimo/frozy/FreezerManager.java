package com.geronimo.frozy;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;

import com.geronimo.frozy.utils.FrozenItemAdapter;
import com.geronimo.frozy.utils.FrozenItemComparator;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Ramona on 26.03.2018.
 */



@Root
public class FreezerManager {
    @ElementList
    private ArrayList<Freezer> _freezerList = new ArrayList<>();

    @Element
    private ItemSortOrder _lastSortOrder = ItemSortOrder.NAME_ASC;

    @ElementList
    private  ArrayList<FrozenItem> _itemList = new ArrayList<>();

    public FreezerManager(){}

    public void autoGenerateFreezer() {
        _freezerList.clear();
        for(int i = 0; i < 3; i++){
            _freezerList.add(new Freezer("Freezer " + (i+1)));

            for(int j = 0; j < 7; j++){
                _freezerList.get(i).addDrawer(new Drawer("Drawer " + (j+1)));
            }
        }
    }

    public  FrozenItem getFrozenItem(int position){
        return _itemList.get(position);
    }

    //returns index where added
    public int addFrozenItem(FrozenItem item){
        _itemList.add(item);
        sortItems(_lastSortOrder);
        return _itemList.indexOf(item);
    }

    public void removeFrozenItem(int position){
        _itemList.remove(position);
        //update recyclerview
        if(Fragment_All._recyclerViewAdapter != null)
            Fragment_All._recyclerViewAdapter.renewList(_itemList);
    }

    public int getFrozenItemCount(){
        return _itemList.size();
    }

    public ArrayList<FrozenItem> getFrozenItemList(){
        return _itemList;
    }

    public  Freezer getFreezer(int index){
        return _freezerList.get(index);
    }

    public  void addFreezer(Freezer freezer){
        _freezerList.add(freezer);
    }

    public  int getFreezerCount(){
        return _freezerList.size();
    }

    public ArrayList<Freezer> getFreezerList() {
        return _freezerList;
    }

    public void sortItems(ItemSortOrder sorting){
        FrozenItemComparator comparator = new FrozenItemComparator(sorting);
        Collections.sort(_itemList,comparator);
        _lastSortOrder = sorting;
        if(Fragment_All._recyclerViewAdapter != null)
            Fragment_All._recyclerViewAdapter.renewList(_itemList);
    }

    public ItemSortOrder getLastSortOrder(){
        return _lastSortOrder;
    }

    public int getNewItemID(){
        boolean idExists = false;
        for(int id = 0; id < Integer.MAX_VALUE; id++){
            idExists = false;
            for(int i = 0; i < getFrozenItemCount(); i++)
            {
                if(_itemList.get(i).getId() == id){
                    idExists = true;
                    break;
                }
            }
            if(!idExists)
                return id;
        }
        return -1;
    }

//    public void sortItems(ItemSortOrder sorting){
//        switch (sorting){
//            case NAME_ASC:
//                sortItemsNameAsc();
//                break;
//            case NAME_DESC:
//                sortItemsNameDesc();
//                break;
//            case DATE_ASC:
//                sortItemsDateAsc();
//                break;
//            case DATE_DESC:
//                sortItemsDateDesc();
//                break;
//        }
//        _lastSortOrder = sorting;
//    }

//    private void sortItemsDateAsc(){
//        Collections.sort(_itemList, new Comparator<FrozenItem>() {
//            @Override
//            public int compare(FrozenItem o1, FrozenItem o2) {
//                return o1.FrozenDate.compareTo(o2.FrozenDate);
//            }
//        });
//    }
//
//    private void sortItemsDateDesc(){
//        Collections.sort(_itemList, new Comparator<FrozenItem>() {
//            @Override
//            public int compare(FrozenItem o1, FrozenItem o2) {
//                return o2.FrozenDate.compareTo(o1.FrozenDate);
//            }
//        });
//    }
//
//    private void sortItemsNameAsc(){
//        Collections.sort(_itemList, new Comparator<FrozenItem>() {
//            @Override
//            public int compare(FrozenItem o1, FrozenItem o2) {
//                return o1.Name.compareTo(o2.Name);
//            }
//        });
//    }
//
//    private void sortItemsNameDesc(){
//        Collections.sort(_itemList, new Comparator<FrozenItem>() {
//            @Override
//            public int compare(FrozenItem o1, FrozenItem o2) {
//                return o2.Name.compareTo(o1.Name);
//            }
//        });
//    }




}
