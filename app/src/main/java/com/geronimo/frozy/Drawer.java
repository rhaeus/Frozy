package com.geronimo.frozy;

import org.simpleframework.xml.Element;

/**
 * Created by Ramona on 24.03.2018.
 */

public class Drawer {
    @Element
    public String Name = "";

    public Drawer(){}

    public Drawer(String name){
        Name = name;
    }
}
